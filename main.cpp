#include <iostream>
#include <string.h>
#include <string>
#include <stdlib.h>
#include "Header.h"
using namespace std;
int main(){
	const int length = 1000;
	char array[length];
	memset(array, '\0', length * sizeof(char));

	int number;
	cout << "Geben Sie Ihre Zahl von 1 bis 5000 ein" << endl;

	do {
		cin >> number;
		if (number > 5000) {
			cout << "Ihre Zahl ist zu gross, geben Sie andere Zahl ein" << endl;
		}
		else if (number < 0) {
			cout << "Ihre Zahl ist kleiner als 0, geben Sie andere Zahl ein" << endl;
		}
	} while (number > 5000 || number < 0);
	rom_num(number, array);
	cout << array << endl;
	system("pause");
	return 0;
}
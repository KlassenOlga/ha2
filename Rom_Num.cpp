#include <iostream>
using namespace std;
int counter = 0;

void process_XX(int number, char* array) {
	int workNumber = number % 100;
	if (workNumber == 95 || workNumber == 45) {
		array[counter++] = 'V';
	}
	if (workNumber == 90) {
		array[counter++] = 'X';
	}
	if (workNumber == 99 || workNumber == 49) {
		array[counter++] = 'I';
	}
}

void rom_num(int number, char* array) {
	//meneaet array napr
	while (number > 0) {
		if (number >= 1000) {
			array[counter++] = 'M';
			number = number - 1000;
		}

		
		else if (number == 990 || number == 995 || number == 999) {
			process_XX(number, array);
			array[counter++] = 'M';
			number = 0;
		}

		else if (number >= 900) {
			array[counter++] = 'C';
			array[counter++] = 'M';
			number = number - 900;

		}

		else if (number >= 500) {
			array[counter++] = 'D';
			number = number - 500;
		}

		else if (number == 495 || number == 499) {
			process_XX(number, array);
			array[counter++] = 'D';
			number = 0;
		}

		else if (number >= 400) {
			array[counter++] = 'C';
			array[counter++] = 'D';
			number = number - 400;

		}
		else if (number >= 100) {
			array[counter++] = 'C';
			number = number - 100;

		}

		else if (number == 95 || number == 99) {
			process_XX(number, array);
			array[counter++] = 'C';
			number = 0;
		}
		else if (number >= 90) {
			array[counter++] = 'X';
			array[counter++] = 'C';
			number = number - 90;
		}

		else if (number >= 50) {
			array[counter++] = 'L';
			number = number - 50;
		}
		else if (number == 45 || number == 49) {
			process_XX(number, array);
			array[counter++] = 'L';
			number = 0;
		}
		else if (number >= 40) {
			array[counter++] = 'X';
			array[counter++] = 'L';
			number = number - 40;
		}
		else if (number >= 10) {
			array[counter++] = 'X';
			number = number - 10;
		}

		else if (number >= 9) {
			array[counter++] = 'I';
			array[counter++] = 'X';
			number = number - 9;
		}
		else if (number >= 5) {

			array[counter++] = 'V';
			number = number - 5;


		}
		else if (number >= 4) {
			array[counter++] = 'I';
			array[counter++] = 'V';
			number = number - 4;

		}
		else if (number >= 1) {
			array[counter++] = 'I';
			number = number - 1;

		}
	}
}